package com.example.alsterboy.psreview;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class news extends Activity
{
    WebView w;
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news);

        w = (WebView) findViewById(R.id.webView);//find webview using id in xml layout file

        w.loadUrl("http://www.playstationlifestyle.net/category/news/");//sets url to webview using loadUrl
    }
}
