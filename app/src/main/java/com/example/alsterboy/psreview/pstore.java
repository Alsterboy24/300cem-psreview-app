package com.example.alsterboy.psreview;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;//import web view

public class pstore extends Activity
    {
        WebView w;//declares webview 2
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ps_store);

            w = (WebView) findViewById(R.id.webView2);//finds webview using the ID from xml file

            w.loadUrl("https://www.playstation.com/en-gb/buy/playstation-store/");//loads the url onto the web view
        }
    }
