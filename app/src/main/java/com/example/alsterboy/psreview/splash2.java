package com.example.alsterboy.psreview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class splash2 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_screen);
        Thread myThread = new Thread()//creates a thread for the splash screen
        {
            @Override
            public void run()//when the file is ran
            {
                try {
                    sleep(5000);//keeps the file for 5 seconds 
                    Intent startHomeScreen = new Intent(getApplicationContext(), HomeActivity.class);//The page will be transitioned to the main page
                    startActivity(startHomeScreen);//start the main page acitvity
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();//otherwise print out the error
                }
            }
        };
        myThread.start();//run the thread
    }
}
