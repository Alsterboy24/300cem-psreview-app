package com.example.alsterboy.psreview;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;

public class SRActivity1 extends Activity
{
    SQLiteDatabase myDB;
     Cursor cursor;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.show_review1);

         File dbFile = new File(getFilesDir() + "/review.db");//creates a db file which gets file review.db
         if (!dbFile.exists())//if the db file doesn't exists
         {
             Log.i("SqlLiteExample", "File doesn't exist");//output message onto datalog

             myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);//open the database after
             myDB.execSQL("create table reviews (name text, game text, review text, rating text, satisfaction text)");//create a table in database review.db

         }
         else// if it does
         {
             Log.i("SqlLiteExample", "File does exist");//output message into datalog
             myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);//open the database
         }



         myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
         cursor = myDB.rawQuery("select * from reviews WHERE game='Horizon Zero Dawn'", null);//selects all the rows from table review where the game is Horizon Zero Dawn

         cursor.moveToFirst();//sets the cursor to the first row
         loadRecord();//calls the function

         final Button p = (Button) findViewById(R.id.previous1);//declares the button using ID from xml file
         p.setOnClickListener(new View.OnClickListener() {//sets a click listener
             public void onClick(View view) {
                 if (cursor.moveToPrevious()) loadRecord();//loads the previous record by moving the cursor backwards
             }
         });

         final Button n = (Button) findViewById(R.id.next1);//declares the button using ID from xml file
         n.setOnClickListener(new View.OnClickListener() {//sets a click listener
             public void onClick(View view) {
                 if (cursor.moveToNext()) loadRecord();//loads the next record by moving cursor forwards
             }
         });
     }

void loadRecord(){//function 

        TextView t1 = (TextView)findViewById(R.id.textViewG1);//gets the textview using ID in xml file
        int game = cursor.getColumnIndex("game");//declares int variable which stores the row from column game
        t1.setText(cursor.getString(game));//sets the text into textfield by converting int to string 

        TextView t2 = (TextView)findViewById(R.id.textViewRA1);//same as the previous button
        int name = cursor.getColumnIndex("name");
        t2.setText(cursor.getString(name));

        TextView t3 = (TextView)findViewById(R.id.textViewR1);//same
        int review = cursor.getColumnIndex("review");
        t3.setText(cursor.getString(review));

        TextView t4 = (TextView)findViewById(R.id.textViewRating1);//same
        int rating = cursor.getColumnIndex("rating");
        t4.setText(cursor.getString(rating));

        TextView t5 = (TextView)findViewById(R.id.textViewRecommend1);//same
        int satisfaction = cursor.getColumnIndex("satisfaction");
        t5.setText(cursor.getString(satisfaction));
    }
}