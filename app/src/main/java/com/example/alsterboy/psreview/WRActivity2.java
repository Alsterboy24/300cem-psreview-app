package com.example.alsterboy.psreview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class WRActivity2 extends Activity
    {
        SQLiteDatabase myDB;
        Cursor cursor;
        RatingBar rb;
        TextView rating;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.write_review2);

        rb=(RatingBar)findViewById(R.id.ratingBar2);
        rating = (TextView)findViewById(R.id.rating);

        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()
        {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
            {
                //rating.setText("Value is" + rating);
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(),Float.toString(rating),Toast.LENGTH_LONG).show();

            }

        });

        AlertDialog alertDialog2 = new AlertDialog.Builder(WRActivity2.this).create(); //Read Update
        alertDialog2.setTitle("Warning");
        alertDialog2.setMessage("The apostrophe (') punctuation is not valid for reviews.");
        alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which){

            }
        });
        alertDialog2.show();

        File dbFile = new File(getFilesDir() + "/review.db");
        if (!dbFile.exists())
        {
            Log.i("SqlLiteExample", "File doesn't exist");

            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
            myDB.execSQL("create table reviews (name text, game text, review text, rating text, satisfaction text)");

        }
        else
        {
            Log.i("SqlLiteExample", "File does exist");
            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
        }

    final Button submit = (Button)findViewById(R.id.submit2);
    submit.setOnClickListener(new View.OnClickListener() {
    public void onClick(View view) {
    insertDB();
        AlertDialog alertDialog = new AlertDialog.Builder(WRActivity2.this).create(); //Read Update
        alertDialog.setTitle("Success");
        alertDialog.setMessage("The review has been submitted.");

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });

        alertDialog.show();
     }
     });
    }


     void insertDB ()
    {
    EditText e1 = (EditText)findViewById(R.id.editTextG2);
    CharSequence game = e1.getText();
    EditText e2 = (EditText)findViewById(R.id.editTextRA2);
    CharSequence name = e2.getText();
    EditText e3 = (EditText)findViewById(R.id.editTextR2);
    CharSequence review = e3.getText();
    RatingBar rb =(RatingBar)findViewById(R.id.ratingBar2);
    Float rating = rb.getRating();
    EditText e5 = (EditText)findViewById(R.id.editTextRecommend2);
    CharSequence satisfaction = e5.getText();



    myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
    myDB.execSQL("insert into reviews values ('" + name + "', '" + game + "', '" + review + "', '" + rating + "', '" + satisfaction + "')");
    myDB.close();
     }


}