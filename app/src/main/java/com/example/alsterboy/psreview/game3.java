package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class game3 extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game3);

        Button plot = (Button)findViewById(R.id.plotButton3);
        plot.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game3.this, plot3.class);
                startActivity(i1);
            }
        });

        Button trailer = (Button)findViewById(R.id.tButton3);
        trailer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game3.this, trailer3.class);
                startActivity(i1);
            }
        });


        Button wreview = (Button)findViewById(R.id.wRButton3);
        wreview.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game3.this, WRActivity3.class);
                startActivity(i1);
            }
        });

        Button sreview = (Button)findViewById(R.id.sRButton3);
        sreview.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game3.this, SRActivity3.class);
                startActivity(i1);
            }
        });


    }

}