package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class game5 extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game5);

        Button plot = (Button)findViewById(R.id.plotButton5);
        plot.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game5.this, plot5.class);
                startActivity(i1);
            }
        });

        Button trailer = (Button)findViewById(R.id.tButton5);
        trailer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game5.this, trailer5.class);
                startActivity(i1);
            }
        });

/*
        Button wreview5 = (Button)findViewById(R.id.button2);
        wreview5.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game5.this, wreview5.class);
                startActivity(i1);
            }
        });

/*        Button sreview5 = (Button)findViewById(R.id.button3);
        sreview5.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game5.this, sreview5.class);
                startActivity(i1);
            }
        });
*/
    }

}