package com.example.alsterboy.psreview;


import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;//imports the youtube variables for a youtube video to be run on the page
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class trailer1 extends YouTubeBaseActivity{// extends the actvity to YoutubeBase so the Android knows it is using Youtube API

YouTubePlayerView youTubePlayerView;
Button b;
YouTubePlayer.OnInitializedListener onInitializedListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trailer1);

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view1);//declares youtube view from xml using ID
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {//sets intial listener on to the youtube view 
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {//when the intialization is successful
                       youTubePlayer.loadVideo("RRQDqurZJNk");//load the video from the link
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        b =  (Button) findViewById(R.id.trailerButton1);
        b.setOnClickListener(new View.OnClickListener() {//sets click listener on button
            @Override
            public void onClick(View v) {
                youTubePlayerView.initialize(Config.YOUTUBE_API_KEY,onInitializedListener);//the youtube view will only be run if the button is clicked, uses the API KEY from file Config
            }//API Key is generated fromd developers console on google
        });
    }


}