package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class game2 extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game2);

        Button plot = (Button)findViewById(R.id.plotButton2);
        plot.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game2.this, plot2.class);
                startActivity(i1);
            }
        });

        Button trailer = (Button)findViewById(R.id.tButton2);
        trailer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game2.this, trailer2.class);
                startActivity(i1);
            }
        });

        Button wreview = (Button)findViewById(R.id.wRButton2);
        wreview.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game2.this, WRActivity2.class);
                startActivity(i1);
            }
        });

        Button sreview = (Button)findViewById(R.id.sRButton2);
        sreview.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game2.this, SRActivity2.class);
                startActivity(i1);
            }
        });


    }

}