package com.example.alsterboy.psreview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class editaccount extends Activity
{
SQLiteDatabase myDB;
Cursor cursor, cursor2;
String user, pass;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editaccount);
        user = global.user;//takes the global variable user from global file
        pass = global.pass;//takes the gloval variable pass from global file

        myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);//opens datatabase
        cursor = myDB.rawQuery("select * from users where username='" + user + "' and password= '" + pass +"'",null);//select query using global variables
        cursor.moveToFirst();
        loadRecord();


        Button Account = (Button) findViewById(R.id.update);
        Account.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                EditText e1 = (EditText) findViewById(R.id.editTextName);
                EditText e2 = (EditText) findViewById(R.id.editTextEmail);
                EditText e3 = (EditText) findViewById(R.id.editTextUsername);
                EditText e4 = (EditText) findViewById(R.id.editTextPassword);

                myDB.execSQL("update users set name='"+e1.getText().toString()+"', email='"+e2.getText().toString()+"', username='"+e3.getText().toString()+"', password='"+e4.getText().toString()+"' where username='"+user+"' and password='"+pass+"'");
                //updates current information by getting the text from edit text fields and converts them into string
                AlertDialog alertDialog = new AlertDialog.Builder(editaccount.this).create(); //Read Update
                alertDialog.setTitle("Success");
                alertDialog.setMessage("Profile has been updated");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which){
                        Intent i = new Intent(editaccount.this, login.class);//when the ok button is clicked on the pop up, it takes the user back to login page
                        startActivity(i);
                    }
                });
                alertDialog.show();
            }
        });


    }

    void loadRecord(){

        EditText e1 = (EditText) findViewById(R.id.editTextName);
        int name = cursor.getColumnIndex("name");
        e1.setText(cursor.getString(name));

        EditText e2 = (EditText) findViewById(R.id.editTextEmail);
        int email = cursor.getColumnIndex("email");
        e2.setText(cursor.getString(email));

        EditText e3 = (EditText)findViewById(R.id.editTextUsername);
        int username = cursor.getColumnIndex("username");
        e3.setText(cursor.getString(username));

        EditText e4 = (EditText) findViewById(R.id.editTextPassword);
        int password = cursor.getColumnIndex("password");
        e4.setText(cursor.getString(password));

    }

}
