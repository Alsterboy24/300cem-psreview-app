package com.example.alsterboy.psreview;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.location.LocationManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleMap mMap2;
    private GoogleMap mMap3;
    private GoogleMap mMap4;

    LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map); //getting the SupportMapFragment by using findFragmentById
        mapFragment.getMapAsync(this); //calling getMapAsync
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            // ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) //check whether the network provider is enabled
        {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() 
            // certain parameters need to be passed: the network/gps provider, minimum time to update
            //minimum distance to update and locationListener.
            //only enabled if the Network Provider is working
            {
                @Override
                public void onLocationChanged(Location location)
                {
                    double latitude = location.getLatitude(); //get the latitude
                    double longitude = location.getLongitude(); //get the longtitude

                    LatLng latLng = new LatLng(latitude, longitude);
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try
                    {
                        List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                        String str = addressList.get(0).getLocality()+",";
                        str += addressList.get(0).getCountryName();
                        mMap.addMarker(new MarkerOptions().position(latLng).title(str));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.2f)); //zooms in on the target Marker by the specified amount
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras)
                {

                }

                @Override
                public void onProviderEnabled(String provider)
                {

                }

                @Override
                public void onProviderDisabled(String provider)
                {

                }
            });
        }
        else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) //check whether the GPS provider is enabled if the network provider is not
        {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener()
            {
                @Override
                public void onLocationChanged(Location location)
                {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    LatLng latLng = new LatLng(latitude, longitude); //instantiate the class, LatLng
                    Geocoder geocoder = new Geocoder(getApplicationContext()); //instantiate the class, Geocoder
                    try
                    {
                        List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1); //get locality + country name from addressList
                        String str = addressList.get(0).getLocality() + ",";
                        str += addressList.get(0).getCountryName();
                        mMap.addMarker(new MarkerOptions().position(latLng).title(str));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.2f));
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras)
                {

                }

                @Override
                public void onProviderEnabled(String provider)
                {

                }

                @Override
                public void onProviderDisabled(String provider)
                {

                }
            });
        }
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
     
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap; //googleMap instance
        mMap2 = googleMap;
        mMap3 = googleMap;
        mMap4 = googleMap;
        LatLng gameStore1 = new LatLng(52.5833383, -2.1291678); //specifying the location for marker placement using latitude and longitude 
        LatLng cexStore1 = new LatLng(52.5854938, -2.1303777);
        LatLng cexStore2 = new LatLng(52.5842972, -2.1297412);
        mMap2.addMarker(new MarkerOptions().position(gameStore1).title("Game Store")); //adding markers on the Map & labelling them
        mMap3.addMarker(new MarkerOptions().position(cexStore1).title("CEX Store 1"));
        mMap4.addMarker(new MarkerOptions().position(cexStore2).title("CEX Store 2"));
        mMap2.moveCamera(CameraUpdateFactory.newLatLng(gameStore1)); //using moveCamera to move to the Marker location specified
        mMap3.moveCamera(CameraUpdateFactory.newLatLng(cexStore1));
        mMap4.moveCamera(CameraUpdateFactory.newLatLng(cexStore2));
    }
}

