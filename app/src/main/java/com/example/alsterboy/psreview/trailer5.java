package com.example.alsterboy.psreview;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class trailer5 extends YouTubeBaseActivity{

YouTubePlayerView youTubePlayerView;
Button b;
YouTubePlayer.OnInitializedListener onInitializedListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trailer5);

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view5);
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                       youTubePlayer.loadVideo("OAXNQveHsG0");
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        b =  (Button) findViewById(R.id.trailerButton5);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youTubePlayerView.initialize(Config.YOUTUBE_API_KEY,onInitializedListener);
            }
        });
    }


}