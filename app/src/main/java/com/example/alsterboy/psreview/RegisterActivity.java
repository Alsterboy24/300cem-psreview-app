package com.example.alsterboy.psreview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;

public class RegisterActivity extends Activity {
    SQLiteDatabase myDB; //`Declaring variable myDB which is the database
    Cursor cursor; //setting a cursor (cursor selects the rows from a database table)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);//sets layout signup.xml

        File dbFile = new File(getFilesDir() + "/psruser.db");//creates a new db file 
        if (!dbFile.exists()) { //if the db doesn't exist
            Log.i("SqlLiteExample", "File doesn't exist");//outputs ithe following message into the datalog
            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);//opens the database
            myDB.execSQL("create table users (name text, email text, username text, password text)");//creates a table called users with the following columns

        } else {
            Log.i("SqlLiteExample", "File does exist"); //if db file exists
            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);//open the database
        }

        final Button submit = (Button) findViewById(R.id.signupButton);//declares a button which is taken from the xml file using the ID
        submit.setOnClickListener(new View.OnClickListener() {//sets click listener
            public void onClick(View view) {//when button is clicked

                insertDB();//do the function insertDB
                AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this).create(); //creates a pop up 
                alertDialog.setTitle("Success");//sets the title of the pop up
                alertDialog.setMessage("User has registered.");//sets message of the pop up

                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {//sets a click listener on the button of the pop up
                    public void onClick(DialogInterface dialog, int which) {
                        // here you can add functions
                    }
                });

                alertDialog.show();//show the pop up
            }

        });
    }



    void insertDB() {//function DB
        EditText e1 = (EditText) findViewById(R.id.editTextName);//find the editText and declares it as e1
        CharSequence name = e1.getText();//get the text from the edit text and sets it into Charsequence so it can be inserted into the database
        EditText e2 = (EditText) findViewById(R.id.editTextEmail);
        CharSequence email = e2.getText();
        EditText e3 = (EditText) findViewById(R.id.editTextUsername);
        CharSequence username = e3.getText();
        EditText e4 = (EditText) findViewById(R.id.editTextPassword);
        CharSequence password = e4.getText();

        myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);//open database
        myDB.execSQL("insert into users values ('" + name + "', '" + email + "', '" + username + "', '" + password + "')");//inserst data into database
        myDB.close();
    }


}
