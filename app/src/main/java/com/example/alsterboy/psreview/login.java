package com.example.alsterboy.psreview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;

public class login extends Activity {
    private EditText Username;
    private EditText Password;
    private Button Login;
    SQLiteDatabase myDB;
    Cursor cursor;
    TextView e1, e2;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        File dbFile = new File(getFilesDir() + "/psruser.db");
        if (!dbFile.exists())
        {
            Log.i("SqlLiteExample", "File doesn't exist");
            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);
            myDB.execSQL("create table users (name text, email text, username text, password text)");

        }
        else
        {
            Log.i("SqlLiteExample", "File does exist");
            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);
        }

        cursor = myDB.rawQuery("select * from users", null);

        cursor.moveToFirst();

        e1 = (TextView)findViewById(R.id.TFusername);
        e2 = (TextView)findViewById(R.id.TFpassword);

        ImageButton login = (ImageButton)findViewById(R.id.imageButtonLogin);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                do {

                    if (cursor.getString(cursor.getColumnIndex("username")).equals(e1.getText().toString()))//uses cursor to check if username in database is equal to the text field
                    {
                        if (cursor.getString(cursor.getColumnIndex("password")).equals(e2.getText().toString())) {//uses cursor to check if password in database is equal to the text field
                            global.user = e1.getText().toString();//sets global variables with the username and password currently used, it will be changed after every login automatically
                            global.pass = e2.getText().toString();
                            Intent i = new Intent(login.this, splash2.class);
                            startActivity(i);
                        }
                        else{
                            AlertDialog alertDialog = new AlertDialog.Builder(login.this).create(); //if the password is wrong pop up with an alert box
                            alertDialog.setTitle("Error");
                            alertDialog.setMessage("wrong username or password");
                            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which){
                                    Intent i = new Intent(login.this, login.class);
                                    startActivity(i);
                                }
                            });
                            alertDialog.show();
                        }
                    }
                    else{
                        AlertDialog alertDialog = new AlertDialog.Builder(login.this).create(); //if the username is wrong pop up with an alert box
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("wrong username or password");
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which){
                                Intent i = new Intent(login.this, login.class);
                                startActivity(i);
                            }
                        });
                        alertDialog.show();

                    }
                } while (cursor.moveToNext());
            }
        });

        ImageButton register = (ImageButton)findViewById(R.id.imageButtonRegister);
        register.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(login.this, RegisterActivity.class);
                startActivity(i1);
            }
        });
    }

}