package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class game4 extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game4);

        Button plot = (Button)findViewById(R.id.plotButton4);
        plot.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game4.this, plot4.class);
                startActivity(i1);
            }
        });

        Button trailer = (Button)findViewById(R.id.tButton4);
        trailer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game4.this, trailer4.class);
                startActivity(i1);
            }
        });

/*
        Button wreview4 = (Button)findViewById(R.id.button2);
        wreview4.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game4.this, wreview4.class);
                startActivity(i1);
            }
        });

/*        Button sreview4 = (Button)findViewById(R.id.button3);
        sreview4.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game4.this, sreview4.class);
                startActivity(i1);
            }
        });
*/
    }

}