package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class HomeActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);//gets the home.xml layout and uses it for HomeActivity
        
        ImageButton Account = (ImageButton) findViewById(R.id.imageButtonTriangle);//Declaring image button
        Account.setOnClickListener(new View.OnClickListener()//setting a click listener so when button is clicked, it does the following
        {
            public void onClick(View v) //when the button is clicked
            {
                Intent i1 = new Intent(HomeActivity.this, MapsActivity.class);//creates a new intent which takes to another page in the app. In this scenario its Map Activity
                startActivity(i1);
            }
        });

        ImageButton Game = (ImageButton)findViewById(R.id.imageButtonCross);
        Game.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i2 = new Intent(HomeActivity.this, game.class);
                startActivity(i2);
            }
        });

        ImageButton Notification = (ImageButton)findViewById(R.id.imageButtonSquare);
        Notification.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i3 = new Intent(HomeActivity.this, news.class);
                startActivity(i3);
            }
        });

        ImageButton Logout = (ImageButton)findViewById(R.id.imageButtonCircle);
        Logout.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i4 = new Intent(HomeActivity.this, login.class);
                startActivity(i4);
            }
        });

        ImageButton account = (ImageButton)findViewById(R.id.imageButtonAccount);
        account.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i5 = new Intent(HomeActivity.this, account.class);
                startActivity(i5);
            }
        });

        ImageButton newgames = (ImageButton)findViewById(R.id.imageButtonNewG);
        newgames.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i6 = new Intent(HomeActivity.this, NewGameActivity.class);
                startActivity(i6);
            }
        });

        ImageButton PStore = (ImageButton)findViewById(R.id.imageButtonStore);
        PStore.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i7 = new Intent(HomeActivity.this, pstore.class);
                startActivity(i7);
            }
        });
    }

}
