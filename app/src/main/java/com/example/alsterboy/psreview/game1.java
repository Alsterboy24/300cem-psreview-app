package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class game1 extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game1);


        Button trailer = (Button)findViewById(R.id.tButton1);
        trailer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game1.this, trailer1.class);
                startActivity(i1);
            }
        });

        Button plot = (Button)findViewById(R.id.plotButton1);
        plot.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game1.this, plot1.class);
                startActivity(i1);
            }
        });

        Button wreview1 = (Button)findViewById(R.id.wRButton1);
        wreview1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game1.this, WRActivity1.class);
                startActivity(i1);
            }
        });

        Button sreview1 = (Button)findViewById(R.id.sRButton1);
        sreview1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game1.this, SRActivity1.class);
                startActivity(i1);
            }
        });

    }
}