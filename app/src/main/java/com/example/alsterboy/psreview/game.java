package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


public class game extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        ImageButton game1 = (ImageButton) findViewById(R.id.imageButton);
        game1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i1 = new Intent(game.this, game1.class);
                startActivity(i1);
            }
        });

        ImageButton game2 = (ImageButton) findViewById(R.id.imageButton2);
        game2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i2 = new Intent(game.this, game2.class);
                startActivity(i2);
            }
        });

        ImageButton game3 = (ImageButton) findViewById(R.id.imageButton3);
        game3.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i3 = new Intent(game.this, game3.class);
                startActivity(i3);
            }
        });

        ImageButton game4 = (ImageButton) findViewById(R.id.imageButton4);
        game4.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i4 = new Intent(game.this, game4.class);
                startActivity(i4);
            }
        });

        ImageButton game5 = (ImageButton) findViewById(R.id.imageButton5);
        game5.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i5 = new Intent(game.this, game5.class);
                startActivity(i5);
            }
        });

        ImageButton game6 = (ImageButton) findViewById(R.id.imageButton6);
        game6.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i6 = new Intent(game.this, game6.class);
                startActivity(i6);
            }
        });

/*        ImageButton game7 = (ImageButton) findViewById(R.id.imageButton7);
        game7.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i7 = new Intent(game.this, game7.class);
                startActivity(i7);
            }
        }); */
    }
}