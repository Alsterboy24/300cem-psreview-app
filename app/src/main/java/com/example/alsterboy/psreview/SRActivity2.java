package com.example.alsterboy.psreview;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

public class SRActivity2 extends Activity
{
    SQLiteDatabase myDB;
     Cursor cursor;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.show_review2);

         File dbFile = new File(getFilesDir() + "/review.db");
         if (!dbFile.exists())
         {
             Log.i("SqlLiteExample", "File doesn't exist");

             myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
             myDB.execSQL("create table reviews (name text, game text, review text, rating text, satisfaction text)");

         }
         else
         {
             Log.i("SqlLiteExample", "File does exist");
             myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
         }



         myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
         cursor = myDB.rawQuery("select * from reviews WHERE game='Nioh'", null);

         cursor.moveToFirst();
         loadRecord();

         final Button p = (Button) findViewById(R.id.previous2);
         p.setOnClickListener(new View.OnClickListener() {
             public void onClick(View view) {
                 if (cursor.moveToPrevious()) loadRecord();
             }
         });

         final Button n = (Button) findViewById(R.id.next2);
         n.setOnClickListener(new View.OnClickListener() {
             public void onClick(View view) {
                 if (cursor.moveToNext()) loadRecord();
             }
         });
     }

void loadRecord(){

        TextView t1 = (TextView)findViewById(R.id.textViewG2);
        int game = cursor.getColumnIndex("game");
        t1.setText(cursor.getString(game));

        TextView t2 = (TextView)findViewById(R.id.textViewRA2);
        int name = cursor.getColumnIndex("name");
        t2.setText(cursor.getString(name));

        TextView t3 = (TextView)findViewById(R.id.textViewR2);
        int review = cursor.getColumnIndex("review");
        t3.setText(cursor.getString(review));

        TextView t4 = (TextView)findViewById(R.id.textViewRating2);
        int rating = cursor.getColumnIndex("rating");
        t4.setText(cursor.getString(rating));

        TextView t5 = (TextView)findViewById(R.id.textViewRecommend2);
        int satisfaction = cursor.getColumnIndex("satisfaction");
        t5.setText(cursor.getString(satisfaction));
    }
}