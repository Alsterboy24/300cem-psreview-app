package com.example.alsterboy.psreview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.RatingBar.OnRatingBarChangeListener;

import java.io.File;

public class WRActivity1 extends Activity
{
    SQLiteDatabase myDB;
    Cursor cursor;
    RatingBar rb;
    TextView rating;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_review1);

        rb=(RatingBar)findViewById(R.id.ratingBar);//declares rating bar by finding the ID
        rating = (TextView)findViewById(R.id.rating);//declares textview by finding ID

        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()//setting change listener on the rating bar
        {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)//when the rating bar changes 
            {
                //rating.setText("Value is" + rating);
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(),Float.toString(rating),Toast.LENGTH_LONG).show();//it sets the rating value onto the textview

            }

        });

        AlertDialog alertDialog2 = new AlertDialog.Builder(WRActivity1.this).create(); //creates pop up
        alertDialog2.setTitle("Warning");
        alertDialog2.setMessage("The apostrophe (') punctuation is not valid for reviews.");
        alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which){

            }
        });
        alertDialog2.show();

        File dbFile = new File(getFilesDir() + "/review.db");
        if (!dbFile.exists())
        {
            Log.i("SqlLiteExample", "File doesn't exist");

            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
            myDB.execSQL("create table reviews (name text, game text, review text, rating float, satisfaction text)");

        }
        else
        {
            Log.i("SqlLiteExample", "File does exist");
            myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
        }

    final Button submit = (Button)findViewById(R.id.submit1);
    submit.setOnClickListener(new View.OnClickListener() {
    public void onClick(View view) {
    insertDB();//calls function
        AlertDialog alertDialog = new AlertDialog.Builder(WRActivity1.this).create(); //Read Update
        alertDialog.setTitle("Success");
        alertDialog.setMessage("The review has been submitted.");

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });

        alertDialog.show();
     }
     });
    }


     void insertDB ()//function for inserting values into the table
    {
        EditText e1 = (EditText)findViewById(R.id.editTextG1);
        CharSequence game = e1.getText();
        EditText e2 = (EditText)findViewById(R.id.editTextRA1);
        CharSequence name = e2.getText();
        EditText e3 = (EditText)findViewById(R.id.editTextR1);
        CharSequence review = e3.getText();
        RatingBar rb =(RatingBar)findViewById(R.id.ratingBar);
        Float rating = rb.getRating();
        EditText e5 = (EditText)findViewById(R.id.editTextRecommend1);
        CharSequence satisfaction = e5.getText();



    myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/review.db", null);
    myDB.execSQL("insert into reviews values ('" + name + "', '" + game + "', '" + review + "', '" + rating + "', '" + satisfaction + "')");
    myDB.close();
     }


}