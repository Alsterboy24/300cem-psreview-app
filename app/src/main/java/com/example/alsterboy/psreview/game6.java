package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class game6 extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game6);

        Button plot = (Button)findViewById(R.id.plotButton6);
        plot.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game6.this, plot6.class);
                startActivity(i1);
            }
        });

        Button trailer = (Button)findViewById(R.id.tButton6);
        trailer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game6.this, trailer6.class);
                startActivity(i1);
            }
        });

/*
        Button wreview6 = (Button)findViewById(R.id.button2);
        wreview6.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game6.this, wreview6.class);
                startActivity(i1);
            }
        });

/*        Button sreview6 = (Button)findViewById(R.id.button4);
        sreview6.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent i1 = new Intent(game6.this, sreview6.class);
                startActivity(i1);
            }
        });
*/
    }

}