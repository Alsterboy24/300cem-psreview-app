package com.example.alsterboy.psreview;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class account extends Activity
{
SQLiteDatabase myDB;
Cursor cursor, cursor2;
String user, pass;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        user = global.user;
        pass = global.pass;

        myDB = SQLiteDatabase.openOrCreateDatabase(getFilesDir() + "/psruser.db", null);
        cursor = myDB.rawQuery("select * from users where username='" + user + "' and password= '" + pass +"'",null);
        cursor.moveToFirst();
        loadRecord();

        Button editAccount = (Button) findViewById(R.id.editAccount);
        editAccount.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i1 = new Intent(account.this, editaccount.class);
                startActivity(i1);
            }
        });


    }

    void loadRecord(){

        TextView t1 = (TextView)findViewById(R.id.textViewname);
        int name = cursor.getColumnIndex("name");
        t1.setText(cursor.getString(name));

        TextView t2 = (TextView)findViewById(R.id.textViewemail);
        int email = cursor.getColumnIndex("email");
        t2.setText(cursor.getString(email));

        TextView t3 = (TextView)findViewById(R.id.textViewusername);
        int username = cursor.getColumnIndex("username");
        t3.setText(cursor.getString(username));

    }

}
