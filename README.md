# **README** #

## **PSReview Repository** ##

* This repository is for the source code i.e Java class files,
* drawable files
* layout xml files
* Android Manifest
* Gradle files etc.

## **PSReview Explained** ##

* This Application was built for the PlayStation gamer, more specifically those who game on the PS4
* The User is able to:
* create an account
* login with that account
* view upcoming games for the PS4 and current trending games
* visit the PlayStation Store via a simple WebView
* access latest news about PlayStation through WebView & view their articles
* view their current location through Google Maps and see Game Store markers
* view information about the trending games, for example game director, programmer etc.
* read basic plot summary of each game.
* watch game trailer
* user can write reviews about these games
* these reviews are saved to a database like the user account details
* all reviews can be viewed (data persistence)
* able to edit account details (username, name, email, password)

## **Source Code [Please Read]** ##

* I had encountered many problems during the development process of PSReview.
* Therefore, I looked to places online like StackOverflow and YouTube for assistance in solving these problems.

## **Software Bugs** ##

* When you login with the correct details, you will encounter the pop up message from when you should enter the wrong username or password
* When editing the account details, upon submitting changes, the app will crash. However, the changes will be saved regardless and you can only login with the new details if you had changed username or password.
* Upon writing a review with apostrophes, the app can crash and it is not known why yet, but as long as there is no usage of that specific punctuation, the review functionality should work fine.

## **Details I missed in the YouTube Demonstration** ##

* The login details are stored on the Database.
* Each review is also stored on the Database.
* Upon closing the application and reopening it, the data will still be existent. (DATA PERSISTENCE)
* The emulator/android device needed Google Play Store services to run the Google Maps Activity.

## **Credit** ##

*My brother assisted in development and solved some critical issues within the application.